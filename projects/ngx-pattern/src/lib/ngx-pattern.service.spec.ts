import { TestBed } from '@angular/core/testing';

import { NgxPatternService } from './ngx-pattern.service';

describe('NgxPatternService', () => {
  let service: NgxPatternService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NgxPatternService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxPatternComponent } from './ngx-pattern.component';

describe('NgxPatternComponent', () => {
  let component: NgxPatternComponent;
  let fixture: ComponentFixture<NgxPatternComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NgxPatternComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NgxPatternComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

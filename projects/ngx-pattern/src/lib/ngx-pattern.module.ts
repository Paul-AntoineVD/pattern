import { NgModule } from '@angular/core';
import { NgxPatternComponent } from './ngx-pattern.component';



@NgModule({
  declarations: [
    NgxPatternComponent
  ],
  imports: [
  ],
  exports: [
    NgxPatternComponent
  ]
})
export class NgxPatternModule { }

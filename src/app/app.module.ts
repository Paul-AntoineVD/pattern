import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GridComponent } from './components/grid/grid.component';
import { GridGroupComponent } from './components/grid-group/grid-group.component';
import { GridColumnComponent } from './components/grid-column/grid-column.component';
import { GridColumnGroupComponent } from './components/grid-column-group/grid-column-group.component';
import { GridDataComponent } from './components/grid-data/grid-data.component';
import { GridDataRowComponent } from './components/grid-data-row/grid-data-row.component';
import { GridResizerComponent } from './components/grid-resizer/grid-resizer.component';

@NgModule({
  declarations: [
    AppComponent,
    GridComponent,
    GridGroupComponent,
    GridColumnComponent,
    GridColumnGroupComponent,
    GridDataRowComponent,
    GridDataComponent,
    GridResizerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { Grid, GridBuilder } from './grid.model';

export class Personne {
  constructor(public prenom: string, public nom: string, public age: number, public equipe: string, public metier: string) {}
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'pattern';

  grid!: Grid;

  ngOnInit() {
    let prenom: string[] = ['Juste', 'Kylian', 'Antoine', 'Olivier', 'N\'Golo', 'Hugo', 'Marcus', 'Zinedine', 'Lilian', 'Didier', 'Eric'];
    let nom: string[] = ['FONTAINE', 'MBAPPÉ', 'GRIEZZMAN', 'GIROUD', 'Kanté', 'Lloris', 'Thuram', 'Zidane', 'Thuram', 'DESCHAMPS', 'CANTONNA'];
    let age: number[] = [23, 24, 29, 32];
    let equipes: string[] = ['Stade de Reims', 'Paris Saint Germain', 'Atletico de Madrid', 'Milan AC', 'unknown', 'unknown','unknown','unknown','unknown','unknown',];
    let metiers: string[] = ['Retraitée', 'Footballeur', 'Footballeur', 'Footballeur', 'unknown','unknown','unknown','unknown','unknown','unknown','unknown',];

    let teams: Personne[] = [];

    for (let i = 0; i < prenom.length; i++)
      teams.push(new Personne(prenom[i], nom[i], age[i], equipes[i], metiers[i]));


    let builder: GridBuilder = {
      properties: [
        {
          key: 'prenom',
          label: 'Prénom',
          type: 'header'
        },
        {
          key: 'nom',
          label: 'Nom',
          type: 'header'
        },
        {
          key: 'age',
          label: 'Age',

          type: 'header',
          secondLabel: 'Somme'
        },
        {
          key: 'equipe',
          label: 'Équipe',
          thirdLabel: 'Code',
          fourthLabel: 'Libelle',
          type: 'header'
        },
        {
          key: 'metier',
          label: 'Métier',
          type: 'header'
        },
        {
          key: 'denomination',
          label: 'Dénomintation',
          type: 'headerGroup',
          child: ['prenom', 'age'],
        },
        {
          key: 'info',
          label: 'Information',
          type: 'headerGroup',
          child: ['equipe, metier']
        }

      ],
      data: teams
    }
    this.grid = new Grid();
    this.grid.columns = [
      {
        key: 'prenom',
        label: 'Prénom',
        height: 24,
        width: 150,
      },
      {
        key: 'nom',
        label: 'Nom',
        height: 24,
        width: 150,
      },
      {
        key: 'age',
        label: 'Age',
        height: 24,
        width: 150,
      }
    ];
    this.grid.data = teams;
    this.grid.columnsGroup = [
      [
        {
          width: 300,
          colSpan: 2,
          label: 'Dénomination'
        },
        {
          label: 'Info',
          width: 150,
          colSpan: 1
        }
      ]
    ]
  }


}

import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { Grid, GridColumnGroupCell } from 'src/app/grid.model';

@Component({
  selector: 'grid-column-group',
  templateUrl: './grid-column-group.component.html',
  styleUrls: ['./grid-column-group.component.css'],
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'grid-column-group'
  }
})
export class GridColumnGroupComponent {

  @Input() rowIndex!: number;
  @Input() columnIndex!: number;

  @Input() grid!: Grid;
  @Output() gridChange = new EventEmitter<Grid>();

  split() {
    let width = this.grid.columnsGroup[this.rowIndex][this.columnIndex].width / this.grid.columnsGroup[this.rowIndex][this.columnIndex].colSpan;

    let newCells: GridColumnGroupCell[] = [];

    for (let i = 0; i < this.grid.columnsGroup[this.rowIndex][this.columnIndex].colSpan ; i++) {
      let newCell: GridColumnGroupCell = {
        colSpan: 1,
        label: this.grid.columnsGroup[this.rowIndex][this.columnIndex].label,
        width: this.grid.columns[this.columnIndex + i].width
      }

      newCells.push(newCell);
    }

    this.grid.columnsGroup[this.rowIndex].splice(this.columnIndex, 1, ...newCells);
  }

  expandLeft() {
    if (this.columnIndex === 0)
      return;

    this.grid.columnsGroup[this.rowIndex][this.columnIndex].colSpan += this.grid.columnsGroup[this.rowIndex][this.columnIndex-1].colSpan;
    this.grid.columnsGroup[this.rowIndex][this.columnIndex].width += this.grid.columnsGroup[this.rowIndex][this.columnIndex-1].width;

    this.grid.columnsGroup[this.rowIndex].splice(this.columnIndex-1, 1);
  }

  expandRight() {
    if (this.columnIndex === this.grid.columnsGroup[this.rowIndex].length - 1)
      return;

    this.grid.columnsGroup[this.rowIndex][this.columnIndex].colSpan += this.grid.columnsGroup[this.rowIndex][this.columnIndex+1].colSpan;
    this.grid.columnsGroup[this.rowIndex][this.columnIndex].width += this.grid.columnsGroup[this.rowIndex][this.columnIndex+1].width;

    this.grid.columnsGroup[this.rowIndex].splice(this.columnIndex+1, 1);
  }
}

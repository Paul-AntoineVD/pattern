import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridDataRowComponent } from './grid-data-row.component';

describe('GridDataComponent', () => {
  let component: GridDataRowComponent;
  let fixture: ComponentFixture<GridDataRowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridDataRowComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GridDataRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { GridCell } from 'src/app/grid.model';

@Component({
  selector: 'grid-data-row',
  templateUrl: './grid-data-row.component.html',
  styleUrls: ['./grid-data-row.component.css'],
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'grid-data-row'
  }
})
export class GridDataRowComponent {

  @Input() gridData!: any;
  @Output() gridDataChange = new EventEmitter<any>();

  @Input() columns!: GridCell[];

  ngOnInit() {
  }
}

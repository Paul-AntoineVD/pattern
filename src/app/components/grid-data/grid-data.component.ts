import { ElementRef, OnInit, ViewEncapsulation } from '@angular/core';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { GridCell } from 'src/app/grid.model';

@Component({
  selector: 'grid-data',
  templateUrl: './grid-data.component.html',
  styleUrls: ['./grid-data.component.css'],
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'grid-data'
  }
})
export class GridDataComponent implements OnInit {

  @Input() column!: GridCell;

  @Input() gridData!: any;
  @Output() gridDataChange = new EventEmitter<any>();

  constructor(private hostElement: ElementRef<HTMLElement>) {}

  ngOnInit() {
    this.hostElement.nativeElement.style.width = `${this.column.width}px`;
  }
}

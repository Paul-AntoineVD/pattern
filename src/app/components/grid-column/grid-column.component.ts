import { Component, ElementRef, EventEmitter, HostBinding, Input, OnChanges, OnInit, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { Grid, GridCell } from 'src/app/grid.model';

@Component({
  selector: 'grid-column',
  templateUrl: './grid-column.component.html',
  styleUrls: ['./grid-column.component.css'],
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'grid-column'
  }
})
export class GridColumnComponent implements OnInit {

  @Input() cellIndex!: number;

  @Input() grid!: Grid;
  @Output() gridChange = new EventEmitter<Grid>();

  @Output() resizeStartEvent = new EventEmitter();

  public constructor(private hostElement: ElementRef<HTMLElement>) {
  }

  ngOnInit() {
    this.buildElement();
  }

  buildElement() {
    this.hostElement.nativeElement.style.width = `${this.grid.columns[this.cellIndex].width}px`;
    this.hostElement.nativeElement.style.minHeight = `${this.grid.columns[this.cellIndex].height}px`;
  }

  resizeStart() {
    this.grid.resizeColumnIndex = this.cellIndex;
    this.gridChange.emit(this.grid);
  }

}

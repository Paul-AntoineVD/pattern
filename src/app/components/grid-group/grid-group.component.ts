import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'grid-group',
  templateUrl: './grid-group.component.html',
  styleUrls: ['./grid-group.component.css'],
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'grid-group'
  }
})
export class GridGroupComponent {

}

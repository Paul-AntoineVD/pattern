import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridResizerComponent } from './grid-resizer.component';

describe('GridResizerComponent', () => {
  let component: GridResizerComponent;
  let fixture: ComponentFixture<GridResizerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridResizerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GridResizerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

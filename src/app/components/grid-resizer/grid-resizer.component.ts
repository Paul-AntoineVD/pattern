import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'grid-resizer',
  templateUrl: './grid-resizer.component.html',
  styleUrls: ['./grid-resizer.component.css'],
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'grid-resizer'
  }
})
export class GridResizerComponent {

}

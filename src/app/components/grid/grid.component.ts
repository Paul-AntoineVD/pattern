import { Component, EventEmitter, HostListener, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { Grid, GridColumnGroupCell } from 'src/app/grid.model';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css'],
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'grid'
  }
})
export class GridComponent implements OnInit {

  @Input() grid!: Grid;
  @Output() gridChange = new EventEmitter<Grid>();


  gridTemplateColumns: string = '';
  gridTemplateRows: string = '';


  ngOnInit() {
  }

  @HostListener('document:mousemove', ['$event'])
  onMouseMove(event: MouseEvent) {
    if (this.grid.resizeColumnIndex === null) {
      return;
    }

    if (this.grid.columns[this.grid.resizeColumnIndex].width + event.movementX >= 32) {
      this.grid.columns[this.grid.resizeColumnIndex].width += event.movementX;

      let indexList = this.grid.getParentIndexColumnGroup(this.grid.resizeColumnIndex);
      for(let i = 0; i < this.grid.columnsGroup.length; i++) {
        this.grid.columnsGroup[i][indexList[i]].width += event.movementX;
      }
    }
  }

  @HostListener('document:mouseup', ['$event'])
  onMouseUp(event: MouseEvent) {
    if (this.grid.resizeColumnIndex !== null) {
      this.grid.resizeColumnIndex = null;
    }
  }

  addColumnGroupRow() {
    let newRow: GridColumnGroupCell[][] = [];
    let newCells: GridColumnGroupCell[] = [];

    this.grid.columns.forEach(c => {
      newCells.push({
        colSpan: 1,
        label: '',
        width: c.width,
      })
    });

    newRow.push(newCells);

    this.grid.columnsGroup = [...newRow, ...this.grid.columnsGroup];
  }

}

export class Grid {
  public columns!: GridCell[];
  public columnsGroup: GridColumnGroupCell[][] = [];
  public dataGroup: GridCell[] = [];
  public edition: boolean = true;
  public total: boolean = true;
  public data: any[] = [];
  public properties: GridProperties[] = [];
  public resize: boolean = false;
  public resizeColumnIndex: number | null = null;

  public static GRID_DEFAULT_HEIGHT = 24;
  public static GRID_DEFAULT_WIDTH = 150;

  constructor(gridBuilder?: GridBuilder)  {
    if (gridBuilder)
      this.buildGrid(gridBuilder);
  }

  private buildGrid(gridBuilder: GridBuilder): Grid {
    this.properties = gridBuilder.properties;
    this.data = gridBuilder.data;

    this.columns = this.buildColumns();
    this.columnsGroup = this.buildColumnsGroup();

    return this;
  }

  private buildColumns(): GridCell[] {
    let columns: GridCell[] = [];

    for (let i = 0; i < this.properties.length; i++) {

      if (this.properties[i].type === 'header') {
        let cell: GridCell = {
          height: Grid.GRID_DEFAULT_HEIGHT,
          width: Grid.GRID_DEFAULT_WIDTH,
          key: this.properties[i].key,
          label: this.properties[i].label,
        }

        if (this.properties[i].secondLabel)
          cell.secondLabel = this.properties[i].secondLabel;

        if (this.properties[i].thirdLabel)
          cell.thirdLabel = this.properties[i].thirdLabel;

        if (this.properties[i].fourthLabel)
          cell.fourthLabel = this.properties[i].fourthLabel;

        columns.push(cell);
      }
    }

    return columns;
  }

  private buildColumnsGroup(): GridColumnGroupCell[][] {
    let columnsGroupRow: GridColumnGroupCell[][] = [];

    let columnsGroup: GridColumnGroupCell[] = [];

    let columnsGroupProperties: GridProperties[] = this.properties.filter(p => p.type === "headerGroup");
    for (let i = 0; i < columnsGroupProperties.length; i++) {

    }

    return columnsGroupRow;
  }

  private getIndexColumnGroup(row: number, colIndex: number): number {
    let colSpan = -1;

    for (let i = 0; i < this.columnsGroup[row].length; i++) {
      colSpan += this.columnsGroup[row][i].colSpan

      if (colSpan >= colIndex) {
        return i;
      }
    }
    return -1;
  }

  public getParentIndexColumnGroup(colIndex: number): number[] {
    let indexList: number[] = [];

    for (let i = 0; i < this.columnsGroup.length; i++) {
      indexList.push(this.getIndexColumnGroup(i, colIndex));
    }

    return indexList;
  }


}

export interface GridBuilder {
  properties: GridProperties[];
  data: any[];
}

export interface GridProperties {
  key: string;
  label: string;
  secondLabel?: string;
  thirdLabel?: string;
  fourthLabel?: string;
  type: GridType;
  child?: string[];
}

export type GridType = 'header' | 'headerGroup' | 'bodyGroup';

export interface GridCell {
  label: string;
  secondLabel?: string,
  thirdLabel?: string;
  fourthLabel?: string;
  code?: string,
  libelle?: string,
  key: string;
  width: number;
  height: number;
  data?: any;
}

export interface GridColumnGroupCell {
  label: string;
  colSpan: number;
  width: number;
}

